package INF101.lab2.pokemon;
import java.lang.Math;
import java.util.Random;
public class Pokemon implements IPokemon {
    private String name;
    private int healthPoints;
    private int maxHealthPoints;
    private int strength;
    private Random random;
    
    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }
    public String getName() { //
        return this.name;
    }
    @Override
    public int getStrength() {
        return this.strength;
    }
    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }
    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }
    public boolean isAlive() {
        return this.healthPoints>0;
    }
    @Override
    public void attack(IPokemon target) {
        System.out.println(this.name + " attacks " + target.getName() +".");
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        target.damage(damageInflicted);
        
        if(target.isAlive() == false) {
            System.out.println(target.getName() + " is defeated by " + this.name);
        }

    }
    @Override
    public void damage(int damageTaken) {
        if(damageTaken>this.healthPoints) {
            this.healthPoints = 0;
        }
        else if (damageTaken<0) {
            return;
        }
        else {
            this.healthPoints = this.healthPoints - damageTaken;
        }
        System.out.println(getName() + " takes " + damageTaken + " damage and is left with " + getCurrentHP()+"/"+getMaxHP()+" HP");
        
    }
    @Override
    public String toString() {
        return getName() + " HP: " + "("+getCurrentHP() + "/" + getMaxHP()+ ") STR: " + getStrength();
    }
}
